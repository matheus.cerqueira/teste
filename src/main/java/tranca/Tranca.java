package tranca;

import java.util.UUID;
import java.util.regex.Pattern;

public class Tranca {
	private String id;
	private String bicicleta;
	private int numero;
	private String localizacao;
	private String anoDeFabricacao;
	private String modelo;
	private String status;
	
	public Tranca(int numero, String localizacao, String anoDeFabricacao, String modelo, StatusT status) {
		
		// Verifica se a localizacao � coordenada
		if (Pattern.compile(
				"^-?(?:[1-9]\\d*|0)(?:.\\d*[0-9])?, -?(?:[1-9]\\d*|0)(?:.\\d*[0-9])?$"
				).matcher(localizacao).matches()
			&& numero > 0) {
			
			this.id = UUID.randomUUID().toString();
			this.bicicleta = null;
			this.numero = numero;
			this.localizacao = localizacao;
			this.anoDeFabricacao = anoDeFabricacao;
			this.modelo = modelo;
			this.status = status.toString();
			
		} else {return;}		
	}

	public String getId() {
		return id;
	}
	
	//Para testes
	public void setId(String id) {
		this.id = id;
	}

	public String getBicicleta() {
		return bicicleta;
	}

	public void setBicicleta(String bicicleta) {
		this.bicicleta = bicicleta;
	}

	public int getNumero() {
		return numero;
	}

	public void setNumero(int numero) {
		if (numero > 0) {
			this.numero = numero;
		}
	}

	public String getLocalizacao() {
		return localizacao;
	}

	public void setLocalizacao(String localizacao) {
		this.localizacao = localizacao;
	}

	public String getAnoDeFabricacao() {
		return anoDeFabricacao;
	}

	public void setAnoDeFabricacao(String anoDeFabricacao) {
		this.anoDeFabricacao = anoDeFabricacao;
	}

	public String getModelo() {
		return modelo;
	}

	public void setModelo(String modelo) {
		this.modelo = modelo;
	}

	public String getStatus() {
		return status;
	}

	public void setStatus(StatusT status) {
		this.status = status.toString();
	}
	
}
