package tranca;

import java.util.ArrayList;
import java.util.Objects;

import bicicleta.*;
import email.EmailController;
import io.javalin.http.Context;
import kong.unirest.json.JSONObject;

public class TrancaController {
	
	public static ArrayList<Tranca> trancas = new ArrayList<Tranca>();
	public static ArrayList<Bicicleta> bicicletas = BicicletaController.bicicletas;
	
	private final static String DADOSINVALIDOS = "Dados Invalidos";
	private final static String NAOENCONTRADO = "Não Encontrado";
	
	// Construtor privado
	private TrancaController() {
		throw new IllegalStateException("TrancaController");
	}
	
	public static void integrarNaRede(Context ctx) {
		
		//acha a tranca pelo id do body
		//checa se o status da tranca � NOVA ou EM_REPARO
		//se sim, tranca fica LIVRE
		//se n�o, erro
		
		String tra = ctx.body();
		JSONObject json = new JSONObject(tra);
		
		String idTranca = json.getString("idTranca");	
		
		try {
			
			for (int i = 0; i < trancas.size(); i++) {
				if (trancas.get(i).getId().equals(idTranca)) {
					
					Tranca tranca = trancas.get(i);
					
					if (tranca.getStatus().equals("NOVA") || tranca.getStatus().equals("EM_REPARO")) {
						
						tranca.setStatus(StatusT.LIVRE);
						EmailController.enviarEmailTrancaReparador(tranca);
						
						ctx.status(200);
						
					}else {throw new IllegalArgumentException();}
				}
			}
			
		} catch (IllegalArgumentException e) {
        	ctx.html(DADOSINVALIDOS);
            ctx.status(422);
		}
		
		//200 "dados cadastrados"
		//422 "dados inv�lidos (ex status inv�lido da tranca), retorna json erro
	}
	
	public static void retirarDaRede(Context ctx) {
		
		//acha a tranca pelo id do body
		//status da tranca n�o pode ser APOSENTADA nem EM_REPARO
		//n�o pode ter tenhuma bicicleta nela
		//muda tranca para EM_REPARO (ou APOSENTADA)
		//se j� estiver com algum desses status ou com uma bicicleta nela, retorna erro
		
		String tra = ctx.body();
		JSONObject json = new JSONObject(tra);
		
		String idTranca = json.getString("idTranca");
		
		try {
			
			for (int i = 0; i < trancas.size(); i++) {
				if (trancas.get(i).getId().equals(idTranca)) {
					
					Tranca tranca = trancas.get(i);
					
					if (tranca.getStatus().equals("APOSENTADA") || 
						tranca.getStatus().equals("EM_REPARO") ||
						tranca.getBicicleta() != null) {
						
						throw new IllegalArgumentException();	
						
					}else {
						tranca.setStatus(StatusT.EM_REPARO); //ou aposentada?
						
						EmailController.enviarEmailTrancaReparador(tranca);
						
						ctx.status(200);
					}
				}
			}
			
		} catch (IllegalArgumentException e) {
        	ctx.html(DADOSINVALIDOS);
            ctx.status(422);
		}
		
		//200 "dados cadastrados"
		//422 "dados inv�lidos (ex status inv�lido da tranca), retorna json erro
	}
	
	public static void recuperaTrancas(Context ctx) {
		
		ctx.json(trancas);
	    ctx.status(200);
	    
		//200 retorna array de jsons
	}
	
	public static void cadastraTranca(Context ctx) {
		
		//Status da tranca dever� ser NOVA
		
		try {
		
			String tra = ctx.body();
			JSONObject json = new JSONObject(tra);
			
			StatusT status = StatusT.NOVA;
			
			Tranca tranca = new Tranca
					(
					 json.getInt("numero"), 
					 json.getString("localizacao"), 
					 json.getString("anoDeFabricacao"), 
					 json.getString("modelo"),
					 status
					);
			
			trancas.add(tranca);
			
			ctx.status(200);
			ctx.json(tranca);
			
		} catch (IllegalArgumentException e) {
	    	ctx.html(DADOSINVALIDOS);
	        ctx.status(422);
		}
					
		//200 "dados cadastrados" retorna o json com a tranca
		//422 "dados invalidos" retorna json erro
	}
	
	public static void getTranca(Context ctx) {
		
		//pega tranca pelo id atrav�s do parametro idTranca
		
		String idTranca = Objects.requireNonNull(ctx.pathParam("idTranca"));
		
		try {
			
			for (int i = 0; i < trancas.size(); i++) {
				if (trancas.get(i).getId().equals(idTranca)) {
					
					Tranca tranca = trancas.get(i);
		        	ctx.status(200);
		        	ctx.json(tranca);
		        	
				}
			}
			
		} catch (NullPointerException n) {
        	ctx.html(NAOENCONTRADO);
            ctx.status(404);
		}
		
		//200 "tranca encontrada", retorna o json da tranca
		//404 "n�o encontrado", retorna json erro
	}
	
	public static void putTranca(Context ctx) {
		
		//pega tranca pelo id atrav�s do parametro idTranca
		//tenta substituir atributos com os que est�o no body
		
		try {
			
			String idTranca = Objects.requireNonNull(ctx.pathParam("idTranca"));
			
			String tra = ctx.body();
			JSONObject json = new JSONObject(tra);
			
			for (int i = 0; i < trancas.size(); i++) {
				if (trancas.get(i).getId().equals(idTranca)) {
					
					Tranca tranca = trancas.get(i);
					
					StatusT status = StatusT.valueOf(json.getString("status"));
					
					tranca.setNumero(json.getInt("numero"));
					tranca.setLocalizacao(json.getString("localizacao"));
					tranca.setAnoDeFabricacao(json.getString("anoDeFabricacao"));
					tranca.setModelo(json.getString("modelo"));
					tranca.setStatus(status);
					
					ctx.html("Dados Atualizados");
		        	ctx.status(200);
		        	ctx.json(tranca);
		        	
				}
			}
			
		} catch (NullPointerException n) {
        	ctx.html(NAOENCONTRADO);
            ctx.status(404);
            
		} catch (IllegalArgumentException e) {
        	ctx.html(DADOSINVALIDOS);
		}    
		
		//200 "dados atualizados", retorna json da tranca
		//404 "n�o encontrado", tranca id nao encontrada, retorna json erro
		//422 "dados invalidos", dados incorretos, atualiza��o nao foi possivel, retorna json erro
	}
	
	public static void deleteTranca(Context ctx) {
		
		//pega tranca pelo id do par�metro idTranca
		//exclui tranca caso ela n�o tenha uma bicicleta
		
		String idTranca = Objects.requireNonNull(ctx.pathParam("idTranca"));
		
		try {
			
			for (int i = 0; i < trancas.size(); i++) {
				
				if (trancas.get(i).getId().equals(idTranca)) {
					
					if (trancas.get(i).getBicicleta() == null) {
						
						trancas.remove(trancas.get(i));
			        	ctx.status(200);
			        	
					} else {throw new IllegalArgumentException();}	        	
				}
			}
			
		} catch (NullPointerException n) {
        	ctx.html(NAOENCONTRADO);
            ctx.status(404);
		} catch (IllegalArgumentException e) {
			ctx.html(DADOSINVALIDOS);
			ctx.status(422);
		}
		
		//200 "Dados removidos"
		//404 "N�o encontrado" retorna json erro
		//422 "Dados invalidos" caso ela ainda tenha uma bicicleta, retorna json erro
	}
	
	public static void getBicicletaTranca(Context ctx) {
		
		//pega tranca pelo id do par�metro idTranca
		//pega a bicicleta que t� no atributo da tranca
		
		String idTranca = Objects.requireNonNull(ctx.pathParam("idTranca"));
		
		try {
			
			for (int i = 0; i < trancas.size(); i++) {
				
				if (trancas.get(i).getId().equals(idTranca)) {
					Tranca tranca = trancas.get(i);
					
					for (int j = 0; j < bicicletas.size(); j++) {
						
						Bicicleta bicicleta = bicicletas.get(j);
						
						if(tranca.getBicicleta().equals(bicicleta.getId())){
				        	ctx.status(200);
				        	ctx.json(bicicleta);
						}
					}
				}
			}
			
		} catch (NullPointerException n) {
        	ctx.html(NAOENCONTRADO);
            ctx.status(404);
		} catch (Exception e) {
        	ctx.html("Id da Tranca inválido");
            ctx.status(422);
		}
		//200 "Tranca encontrada", retorna json da bicicleta
		//404 "Bicicleta n�o encontrada", json erro
		//422 "Id da Tranca inv�lido", json erro
	}
	
	
	public static void alteraStatus (Context ctx) {
		
		//pega tranca pelo id do par�metro idTranca
		//muda o status com o que est� no par�metro a�ao
		
		try {
			
			String idTranca = Objects.requireNonNull(ctx.pathParam("idTranca"));
			
			String acao = Objects.requireNonNull(ctx.pathParam("acao"));
			
			if (acao.equals("TRANCAR")) { acao = "OCUPADA";}
			else if (acao.equals("DESTRANCAR")) { acao = "LIVRE";}
			
			
			for (int i = 0; i < trancas.size(); i++) {
				if (trancas.get(i).getId() != null && trancas.get(i).getId().equals(idTranca)) {
					
					Tranca tranca = trancas.get(i);
					
					StatusT status = StatusT.valueOf(acao);
					
					tranca.setStatus(status);
					
					ctx.html("Dados Atualizados");
		        	ctx.status(200);
		        	ctx.json(tranca);
		        	
				}
			}
			
		} catch (NullPointerException n) {
        	ctx.html(NAOENCONTRADO);
            ctx.status(404);
            
		} catch (Exception e) {
        	ctx.html(DADOSINVALIDOS);
		}  
		
		//200 "a�ao bem sucedida", retorna json da tranca atualizada.
		//404 "n�o encontrado", retorna json erro
		//422 "dados inv�lidos", o status n�o faz sentido, retorna json erro
	}	
	
}
