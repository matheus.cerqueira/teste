package bicicleta;

import java.time.Year;
import java.util.UUID;

public class Bicicleta {
	
	private String id;
	private String marca;
	private String modelo;
	private String ano;
	private int numero;
	private String status;
	
	public Bicicleta(String marca, String modelo, Year ano, int numero, Status status) {
		this.id = UUID.randomUUID().toString();
		this.marca = marca;
		this.modelo = modelo;
		this.ano = String.valueOf(ano);
		this.numero = numero;
		this.status = status.toString();
	}

	public String getMarca() {
		return marca;
	}
	
	public void setMarca(String marca) {
		this.marca = marca;
	}
	
	public String getModelo() {
		return modelo;
	}
	
	public void setModelo(String modelo) {
		this.modelo = modelo;
	}
	
	public String getAno() {
		return ano;
	}
	
	public void setAno(Year ano) {
		this.ano = String.valueOf(ano);
	}
	
	public int getNumero() {
		return numero;
	}
	
	public void setNumero(int numero) {
		this.numero = numero;
	}
	
	public String getStatus() {
		return status;
	}
	
	public void setStatus(Status status) {
		this.status = status.toString();
	}

	public String getId() {
		return id;
	}
	
	public void setId(String id) {
		this.id = id;
	}
	
}
