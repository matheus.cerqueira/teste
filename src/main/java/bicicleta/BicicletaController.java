package bicicleta;

import java.time.Year;

import java.util.ArrayList;

import tranca.*;

import email.*;

import io.javalin.http.Context;
import kong.unirest.json.JSONObject;
import java.util.Objects;

public class BicicletaController {
	
	public static ArrayList<Bicicleta> bicicletas = new ArrayList<Bicicleta>();
	public static ArrayList<Tranca> trancas = TrancaController.trancas;
	
	private final static String BIKEID = "idBicicleta";
	private final static String DADOSINVALIDOS = "Dados Invalidos";
	private final static String NAOENCONTRADO = "Não Encontrado";
	
	// Construtor privado
	private BicicletaController() {
		throw new IllegalStateException("BicicletaController");
	}
	
	// Recupera lista de bicicletas
	public static void recuperaBicicletas(Context ctx) {
		
		ctx.json(bicicletas);
	    ctx.status(200);
		
		//200 retorna array de jsons
	}
	
	// Cadastra uma bicicleta
	public static void cadastraBicicleta(Context ctx) {
		
		//Status da bicicleta dever� ser NOVA
		
		try {
			
			String bike = ctx.body();
			JSONObject json = new JSONObject(bike);
		
		
			int ano = Integer.parseInt(json.getString("ano"));
			Year year = Year.of(ano);
			
			Status status = Status.NOVA;
			
			Bicicleta bicicleta = new Bicicleta
					(
					 json.getString("marca"), 
					 json.getString("modelo"), 
					 year, 
					 json.getInt("numero"),
					 status
					);
			
			bicicletas.add(bicicleta);
			
			ctx.status(200);
			ctx.json(bicicleta);
			
		} catch (IllegalArgumentException e) {
        	ctx.html(DADOSINVALIDOS);
            ctx.status(422);
		}
		
		//200 "dados cadastrados" retorna o json
		//422 "dados invalidos" retorna json erro
	}
	
	public static void integrarNaRede(Context ctx) {
		
		//acha a bicicleta e a tranca pelo id
		//checa se o status da bicicleta � NOVA ou EM_REPARO, se tranca est� com status LIVRE
		//se sim, bicicleta fica DISPONIVEL e tranca OCUPADA
		//se n�o, erro

		String bike = ctx.body();
		JSONObject json = new JSONObject(bike);
		
		String idBicicleta = json.getString(BIKEID);
		String idTranca = json.getString("idTranca");	
		
		try {
			
			Bicicleta bicicleta = checarNovaOuReparo(idBicicleta);
			
			if(bicicleta == null) {throw new IllegalArgumentException();}
			
			Tranca tranca = checarLivre(idTranca);
			
			if(tranca == null) {throw new IllegalArgumentException();}
			
			bicicleta.setStatus(Status.DISPONIVEL);
			tranca.setStatus(StatusT.OCUPADA);
			
			tranca.setBicicleta(bicicleta.getId());
			
			EmailController.enviarEmailBicicletaReparador(bicicleta, tranca);
			
			ctx.status(200);
			
		} catch (IllegalArgumentException e) {
        	ctx.html(DADOSINVALIDOS);
            ctx.status(422);
		}
		
		//200 "dados cadastrados"
		//422 "dados inv�lidos (ex status inv�lido da bicicleta ou tranca), retorna json erro
	}
	
	private static Bicicleta checarNovaOuReparo(String idBicicleta) {
	
		
		for (int i = 0; i < bicicletas.size(); i++) {
			if (bicicletas.get(i).getId().equals(idBicicleta)) {
				
				Bicicleta bicicleta = bicicletas.get(i);
				
				if (bicicleta.getStatus().equals("NOVA") || bicicleta.getStatus().equals("EM_REPARO")) {
					return bicicleta;
				}
	        	
			}
		}
		return null;
	}
	
	private static Tranca checarLivre(String idTranca) {
		
		for (int i = 0; i < trancas.size(); i++) {
			if (trancas.get(i).getId().equals(idTranca)) {
				
				Tranca tranca = trancas.get(i);
				
				if (tranca.getStatus().equals("LIVRE")) {
					return tranca;
				}
			}
		}
		
		return null;
	}
	
	public static void retirarDaRede(Context ctx) {
		
		//acha a tranca pelo id e checa se o id da bicicleta � o que t� nela
		//checa se o status da bicicleta é REPARO_SOLICITADO e tranca esteja OCUPADA
		//se sim, bicicleta fica EM_REPARO (ou APOSENTADA) e tranca LIVRE
		//se n�o, erro

		String bike = ctx.body();
		JSONObject json = new JSONObject(bike);
		
		String idBicicleta = json.getString(BIKEID);
		String idTranca = json.getString("idTranca");
		
		try {
			
			Tranca tranca = checarTranca(idTranca);
			
			if(tranca == null) {throw new IllegalArgumentException();}
			if(!tranca.getBicicleta().equals(idBicicleta)){throw new IllegalArgumentException();}
			
			Bicicleta bicicleta = checarBicicleta(idBicicleta);
			
			if(bicicleta == null) {throw new IllegalArgumentException();}
			
			if (bicicleta.getStatus().equals("REPARO_SOLICITADO") && tranca.getStatus().equals("OCUPADA")) {
				
				bicicleta.setStatus(Status.EM_REPARO); //ou aposentada?
				tranca.setStatus(StatusT.LIVRE);
				
				EmailController.enviarEmailBicicletaReparador(bicicleta, tranca);
				
				ctx.status(200);
			}
			
		} catch (IllegalArgumentException e) {
        	ctx.html(DADOSINVALIDOS);
            ctx.status(422);
		}
		
		//200 "dados cadastrados"
		//422 "dados inv�lidos (ex status inv�lido da bicicleta ou tranca), retorna json erro
	}
	
	private static Tranca checarTranca(String idTranca) {
		
		for (int i = 0; i < trancas.size(); i++) {
			if (trancas.get(i).getId().equals(idTranca)) {
				
				Tranca tranca = trancas.get(i);
				return tranca;
			}
		}
		return null;
	}
	
	private static Bicicleta checarBicicleta(String idBicicleta) {

		for (int i = 0; i < bicicletas.size(); i++) {
			if (bicicletas.get(i).getId().equals(idBicicleta)) {
				
				Bicicleta bicicleta = bicicletas.get(i);
				
				return bicicleta;
			}
		}
		return null;
	}
	
	public static void getBicicleta(Context ctx) {
		
		//pega bicicleta pelo id atraves do parametro idBicicleta
		
		String bicicletaId = Objects.requireNonNull(ctx.pathParam(BIKEID));
		
		try {
			
			for (int i = 0; i < bicicletas.size(); i++) {
				if (bicicletas.get(i).getId().equals(bicicletaId)) {
					
					Bicicleta bicicleta = bicicletas.get(i);
		        	ctx.status(200);
		        	ctx.json(bicicleta);
		        	return;
		        	
				}
			}
			
			throw new NullPointerException();
			
		} catch (NullPointerException n) {
        	ctx.html(NAOENCONTRADO);
            ctx.status(404);
		}
		
		
		//200 "bicicleta encontrada", retorna o json da bicicleta
		//404 "nao encontrado", retorna json erro
	}
	
	public static void putBicicleta(Context ctx) {
		
		//pega bicicleta pelo id atrav�s do parametro idBicicleta
		//tenta substituir atributos com os que est�o no body, menos status
		try {
			
			String bicicletaId = Objects.requireNonNull(ctx.pathParam(BIKEID));
			
			String bike = ctx.body();
			JSONObject json = new JSONObject(bike);
			
			for (int i = 0; i < bicicletas.size(); i++) {
				if (bicicletas.get(i).getId().equals(bicicletaId)) {
					
					Bicicleta bicicleta = bicicletas.get(i);
					
					int ano = Integer.parseInt(json.getString("ano"));
					Year year = Year.of(ano);
										
					bicicleta.setMarca(json.getString("marca"));
					bicicleta.setModelo(json.getString("modelo"));
					bicicleta.setAno(year);
					bicicleta.setNumero(json.getInt("numero"));
					
					ctx.html("Dados Atualizados");
		        	ctx.status(200);
		        	ctx.json(bicicleta);
		        	
				}
			}
			
		} catch (NullPointerException n) {
        	ctx.html(NAOENCONTRADO);
            ctx.status(404);
            
		} catch (IllegalArgumentException e) {
        	ctx.html(DADOSINVALIDOS);
            ctx.status(422);		
		}
		
		//200 "dados atualizados", retorna json da bicicleta
		//404 "n�o encontrado", bicicleta id nao encontrada, retorna json erro
		//422 "dados invalidos", dados incorretos, atualiza��o nao foi possivel, retorna json erro
	}
	
	public static void deleteBicicleta(Context ctx) {
		
		//pega bicicleta pelo id do par�metro idBicicleta
		//exclui bicicleta
		
		String bicicletaId = Objects.requireNonNull(ctx.pathParam(BIKEID));
		
		try {
			
			for (int i = 0; i < bicicletas.size(); i++) {
				if (bicicletas.get(i).getId().equals(bicicletaId)) {
					
					bicicletas.remove(bicicletas.get(i));
					
		        	ctx.status(200);
				}
			}
			
		} catch (NullPointerException n) {
        	ctx.html(NAOENCONTRADO);
            ctx.status(404);
		}		
		
		//200 "Dados removidos"
		//404 "N�o encontrado" retorna json erro
	}
	
	public static void alteraStatus(Context ctx) {
		
		//pega bicicleta pelo id do par�metro idBicicleta
		//muda o status com o que est� no par�metro a�ao
		
		String bicicletaId = Objects.requireNonNull(ctx.pathParam("id"));
		
		String acao = Objects.requireNonNull(ctx.pathParam("acao"));
		
		try {
			
			for (int i = 0; i < bicicletas.size(); i++) {
				if (bicicletas.get(i).getId().equals(bicicletaId)) {
					
					Bicicleta bicicleta = bicicletas.get(i);
					
					Status status = Status.valueOf(acao);
					
					bicicleta.setStatus(status);
					
		        	ctx.status(200);
		        	ctx.json(bicicleta);
				}
			}
			
		} catch (NullPointerException n) {
        	ctx.html(NAOENCONTRADO);
            ctx.status(404);
		} catch (IllegalArgumentException e) {
        	ctx.html(DADOSINVALIDOS);
            ctx.status(422);
		}
		
		//200 "a�ao bem sucedida", retorna json da bicicleta atualizada.
		//404 "n�o encontrado", retorna json erro
		//422 "dados inv�lidos", o status n�o faz sentido, retorna json erro
	}
}