package totem;

import java.util.ArrayList;
import java.util.Objects;

import tranca.*;
import bicicleta.*;

import io.javalin.http.Context;
import kong.unirest.json.JSONObject;

public class TotemController {
	
	public static ArrayList<Totem> totens = new ArrayList<Totem>();
	public static ArrayList<Tranca> trancas = TrancaController.trancas;
	public static ArrayList<Bicicleta> bicicletas = BicicletaController.bicicletas;
	
	private final static String TOTEMID = "idTotem";
	private final static String DADOSINVALIDOS = "Dados Invalidos";
	private final static String NAOENCONTRADO = "Não Encontrado";
	
	// Construtor privado
	private TotemController() {
		throw new IllegalStateException("TotemController");
	}
	
	public static void recuperaTotens(Context ctx) {
		
		ctx.json(totens);
	    ctx.status(200);
		
		//200 retorna array de jsons
	}
	
	public static void cadastraTotem(Context ctx) {
		
		try {
			
			String tot = ctx.body();
			JSONObject json = new JSONObject(tot);
			
			Totem totem = new Totem
					(
					 json.getString("localizacao")
					);
			
			totens.add(totem);
			
			ctx.status(200);
			ctx.json(totem);
			
		} catch (Exception e) {
        	ctx.html(DADOSINVALIDOS);
            ctx.status(422);
		}
		
		//200 "dados cadastrados" retorna o json do totem
		//422 "dados invalidos" retorna json erro
	}
	
	public static void putTotem(Context ctx) {
		
		//pega totem pelo id atrav�s do parametro idTotem
		//tenta substituir atributo com o que est� no body
		
		try {
			
			String idTotem = Objects.requireNonNull(ctx.pathParam(TOTEMID));
			
			String tot = ctx.body();
			JSONObject json = new JSONObject(tot);
			
			for (int i = 0; i < totens.size(); i++) {
				if (totens.get(i).getId().equals(idTotem)) {
					
					Totem totem = totens.get(i);
										
					totem.setLocalizacao(json.getString("localizacao"));
					
					ctx.html("Dados Atualizados");
		        	ctx.status(200);
		        	ctx.json(totem);
		        	
				}
			}
			
		} catch (NullPointerException n) {
        	ctx.html(NAOENCONTRADO);
            ctx.status(404);
            
		} catch (Exception e) {
        	ctx.html(DADOSINVALIDOS);
            ctx.status(422);		
		}
		
		//200 "dados atualizados", retorna json do totem atualizado
		//404 "n�o encontrado", totem id nao encontrada, retorna json erro
		//422 "dados invalidos", dados incorretos, atualiza��o nao foi possivel, retorna json erro
	}
	
	public static void deleteTotem(Context ctx) {
		
		//pega totem pelo id do par�metro idTotem
		//exclui totem
		
		String idTotem = Objects.requireNonNull(ctx.pathParam(TOTEMID));
		
		try {
			
			for (int i = 0; i < totens.size(); i++) {
				if (totens.get(i).getId().equals(idTotem)) {
					
					totens.remove(totens.get(i));
					
		        	ctx.status(200);
				}
			}
			
		} catch (NullPointerException n) {
        	ctx.html(NAOENCONTRADO);
            ctx.status(404);
		}		
		
		//200 "Dados removidos"
		//404 "N�o encontrado"
	}
	
	public static void getTrancasTotem(Context ctx) {
		
		//pega totem pelo id do parametro idTotem
		//pega a localizacao
		//pega todos as trancas e compara a localiza�ao deles
		//pega todos com a mesma localiza�ao
		
		String idTotem = Objects.requireNonNull(ctx.pathParam(TOTEMID));
		ArrayList<Tranca> trancasTotem = new ArrayList<Tranca>();
		
		try {
			
			for (int i = 0; i < totens.size(); i++) {
				if (totens.get(i).getId().equals(idTotem)) {
					
					Totem totem = totens.get(i);
					
					for (int j = 0; j < trancas.size(); j++) {
						if (trancas.get(j).getLocalizacao().equals(totem.getLocalizacao())) {
							
							trancasTotem.add(trancas.get(j));
						}
					}			
					
					ctx.status(200);
		        	ctx.json(trancasTotem);
					
				}
			}
			
		} catch (NullPointerException n) {
        	ctx.html(NAOENCONTRADO);
            ctx.status(404);
		} catch (IllegalArgumentException e) {
        	ctx.html(DADOSINVALIDOS);
            ctx.status(422);
		}
		
		//200 OK, retorna array de json das trancas
		//404 N�o encontrado, json erro
		//422 "dados incorretos"? json erro
	}
	
	public static void getBicicletasTotem(Context ctx) {
		//pega totem pelo id do parametro idTotem
		//pega a localizacao
		//pega todos as trancas e compara a localiza�ao deles
		//em cada um com a mesma localiza��o, pega o id da bicicleta
		//guarda as bicicletas pertencentes �s trancas com a mesma localizacao
		
		String idTotem = Objects.requireNonNull(ctx.pathParam(TOTEMID));
		ArrayList<Bicicleta> bicicletasTotem = new ArrayList<Bicicleta>();
		Totem totem = null;
		Tranca tranca = null;
		
		try {
			
			for (int i = 0; i < totens.size(); i++) {
				if (totens.get(i).getId().equals(idTotem)) {
					
					totem = totens.get(i);
					break;
				}
			}
			
			for (int i = 0; i < trancas.size(); i++) {
				
				tranca = trancas.get(i);
				
				if (tranca.getLocalizacao().equals(totem.getLocalizacao())) {
					
					for (int j = 0; j < bicicletas.size(); j++) {
						
						if (tranca.getBicicleta().equals(bicicletas.get(j).getId())) {
							bicicletasTotem.add(bicicletas.get(j));
						}
					}

				}
			}			
			
			ctx.status(200);
        	ctx.json(bicicletasTotem);
        	
		} catch (NullPointerException n) {
        	ctx.html(NAOENCONTRADO);
            ctx.status(404);
		} catch (Exception e) {
        	ctx.html(DADOSINVALIDOS);
            ctx.status(422);
		}
		
		//200 OK, retorna array de json das bicicletas
		//404 N�o encontrado, json erro
		//422 "dados incorretos"? json erro
	}
}
