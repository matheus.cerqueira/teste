package totem;

import java.util.UUID;
import java.util.regex.Pattern;

public class Totem {
			
	private String id;
	private String localizacao;
	
	public Totem(String localizacao){
		
		// Verifica (usando regex) se a localiza��o � coordenada
		if (Pattern.compile(
				"^-?(?:[1-9]\\d*|0)(?:.\\d*[0-9])?, -?(?:[1-9]\\d*|0)(?:.\\d*[0-9])?$"
				).matcher(localizacao).matches()
			) {
			
			this.id = UUID.randomUUID().toString();
			this.localizacao = localizacao;
			
		} else {return;}
	}

	public String getLocalizacao() {
		return localizacao;
	}

	public void setLocalizacao(String localizacao) {
		this.localizacao = localizacao;
	}

	public String getId() {
		return id;
	}
	
	//Para testes
	public void setId(String id) {
		this.id = id;
	}
}
