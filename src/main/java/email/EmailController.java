package email;

import io.javalin.http.Context;
import kong.unirest.HttpResponse;
import kong.unirest.JsonNode;
import kong.unirest.Unirest;
import kong.unirest.json.JSONObject;
import bicicleta.*;
import tranca.*;

public class EmailController {
	
	private static final String INTEGRACAO_EMAIL = "https://va-de-bicicleta.herokuapp.com/enviarEmail";
	private static final String INTEGRACAO_FUNCIONARIO = "https://pm-2021-2-jvaf.herokuapp.com/funcionario/";
	private static final String ID_REPARADOR = "43a3b575-459e-45b7-9c1e-54521edd3973";
	
	private static String enviarEmail(String email, String mensagem) {
		
    	HttpResponse<JsonNode> response = Unirest.post(INTEGRACAO_EMAIL).body(
    			"{\"email\":\""+ email +"\", "
    			+ "\"mensagem\":\""+ mensagem +"\""
    			+ "}").asJson();
		
		if (response.isSuccess()) {
			return "200";
		}
		
		return "422";
	}
	
	public static String enviarEmailBicicletaReparador(Bicicleta biciclet, Tranca tranc) {
		
		String mensagem = "A bicicleta " + biciclet.getId() + " mudou seu status para " +
						 biciclet.getStatus() + " e a tranca " + tranc.getId() +
						 " mudou seu status para " + tranc.getStatus() + ".";
		
		HttpResponse<String> reparador = Unirest.get(INTEGRACAO_FUNCIONARIO + ID_REPARADOR).asString();
		
		return enviarEmail("reparador@mail.com", mensagem);
	}
	
	public static String enviarEmailTrancaReparador(Tranca tranc) {
		
		String mensagem = "A tranca " + tranc.getId() + " mudou seu status para " + tranc.getStatus() + ".";
		
		HttpResponse<String> reparador = Unirest.get(INTEGRACAO_FUNCIONARIO + ID_REPARADOR).asString();
		
		return enviarEmail("reparador@mail.com", mensagem);
	}

}