package echo;

import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.Mockito.*;
import java.time.Year;

import org.junit.jupiter.api.AfterAll;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Test;

import java.util.ArrayList;

import main.util.JavalinApp;
import kong.unirest.HttpResponse;
import kong.unirest.JsonNode;
import kong.unirest.Unirest;
import org.mockito.Mock;
import io.javalin.http.Context;
import io.javalin.plugin.json.JavalinJson;
import bicicleta.*;
import totem.*;
import tranca.*;

class ControllerTest {

    private static JavalinApp app = new JavalinApp();
    public static final int defaultPort = 7010;
    public static final String baseUrl = "http://localhost:7010/";
    
    @BeforeAll
    static void init() {
        app.start(7010);
    }

    @AfterAll
    static void afterAll(){
        app.stop();
    }
    
    @Test
    public void recuperaBicicletasTest() {
    	
    	getBicicletaNova();
    	
    	HttpResponse<String> response = Unirest.get(baseUrl+"bicicleta").asString();        
        assertEquals(200, response.getStatus());  
        assertEquals(JavalinJson.toJson(BicicletaController.bicicletas), response.getBody());
    } 
    
    @Test
    public void getBicicletaTest() {
    	
    	Bicicleta bike = getBicicletaNova();
    	
    	HttpResponse<String> response = Unirest.get(baseUrl+"bicicleta/" + bike.getId()).asString(); 
        assertEquals(200, response.getStatus());  
        assertEquals(JavalinJson.toJson(bike), response.getBody());
    	/*
        when(ctx.queryParam("idBicicleta")).thenReturn(bike.getId());
        BicicletaController.getBicicleta(ctx);
        verify(ctx).status(200);*/
    }
    
    @Test
    public void cadastraBicicletaTest() {
    	Bicicleta bike = getBicicletaNova();
    	
    	HttpResponse<JsonNode> response = Unirest.post(baseUrl+"bicicleta").body(
    			"{\"marca\":\""+ bike.getMarca() +"\", "
    			+ "\"modelo\":\""+ bike.getModelo() +"\", "
    			+ "\"ano\":\""+ bike.getAno() +"\", "
    			+ "\"numero\":\""+ bike.getNumero() +"\""
    			+ "}").asJson();
    	
    	Bicicleta bike2 = getBicicletaNova();
    	bike2.setStatus(Status.NOVA);
    	//Colocar o mesmo id do que foi criado no post
    	Bicicleta bicicleta = BicicletaController.bicicletas.get(BicicletaController.bicicletas.size()-2);	
    	bike2.setId(bicicleta.getId());
    	
    	assertEquals(200, response.getStatus());
        assertEquals(JavalinJson.toJson(bike2), response.getBody().toString());
    }
    
    @Test
    public void putBicicletaTest() {
    	
    	Bicicleta bike = getBicicletaNova();
    	
    	HttpResponse<JsonNode> response = Unirest.put(baseUrl+"bicicleta/" + bike.getId()).body(
    			"{\"marca\":\""+ bike.getMarca() +"\", "
    			+ "\"modelo\":\""+ bike.getModelo() +"\", "
    			+ "\"ano\":\""+ bike.getAno() +"\", "
    			+ "\"numero\":\""+ 13 +"\""
    			+ "}").asJson();
    	
    	Bicicleta bike2 = getBicicletaNova();
    	bike2.setId(bike.getId());
    	bike2.setNumero(13);
    	
        assertEquals(200, response.getStatus());  
        assertEquals(JavalinJson.toJson(bike2), response.getBody().toString());
    } 
    
    @Test
    public void deleteBicicletaTest() {
        
    	Bicicleta bike = getBicicletaNova();
    	
    	HttpResponse<String> response = Unirest.delete(baseUrl+"bicicleta/" + bike.getId()).asString(); 
        assertEquals(200, response.getStatus());
    }
    
    @Test
    public void integraBicicletaNaRedeTest() {
        
    	Bicicleta bike = getBicicletaNova();
    	Tranca tra = getTrancaNova();
    	
    	tra.setBicicleta(bike.getId());
    	tra.setStatus(StatusT.LIVRE);
    	bike.setStatus(Status.NOVA);
    	
    	HttpResponse<JsonNode> response = Unirest.post(baseUrl+"bicicleta/integrarNaRede").body(
    			"{\"idBicicleta\":\""+ bike.getId() +"\", "
    			+ "\"idTranca\":\""+ tra.getId() +"\" "
    			+ "}").asJson();
    	
    	/*
    	Bicicleta bike2 = getBicicletaNova();
    	Tranca tra2 = getTrancaNova();
    	
    	tra2.setBicicleta(bike2.getId());
    	tra2.setStatus(StatusT.OCUPADA);
    	bike2.setStatus(Status.DISPONIVEL);*/
    	
        assertEquals(200, response.getStatus());
    }
    
    @Test
    public void retiraBicicletaDaRedeTest() {
    	
    	Bicicleta bike = getBicicletaNova();
    	Tranca tra = getTrancaNova();
    	
    	tra.setBicicleta(bike.getId());
    	tra.setStatus(StatusT.OCUPADA);
    	bike.setStatus(Status.REPARO_SOLICITADO);
    	
    	HttpResponse<JsonNode> response = Unirest.post(baseUrl+"bicicleta/retirarDaRede").body(
    			"{\"idBicicleta\":\""+ bike.getId() +"\", "
    			+ "\"idTranca\":\""+ tra.getId() +"\" "
    			+ "}").asJson();
    	
        assertEquals(200, response.getStatus());
    }
    
    @Test
    public void alterarStatusBicicletaTest() {
    	
    	Bicicleta bike = getBicicletaNova();
    	
    	HttpResponse<String> response = Unirest.post(baseUrl+"bicicleta/{id}/status/{acao}")
        .routeParam("id", bike.getId())
        .routeParam("acao", "EM_USO")
        .asString();
        
    	Bicicleta bike2 = getBicicletaNova();
    	bike2.setId(bike.getId());
    	bike2.setStatus(Status.EM_USO);
    	
    	assertEquals(200, response.getStatus());  
        assertEquals(JavalinJson.toJson(bike2), response.getBody());
    }
    
    @Test
    public void recuperaTrancasTest() {
    	
    	getTrancaNova();
    	
    	HttpResponse<String> response = Unirest.get(baseUrl+"tranca").asString();        
        assertEquals(200, response.getStatus());  
        assertEquals(JavalinJson.toJson(TrancaController.trancas), response.getBody());
    } 
    
    @Test
    public void getTrancaTest() {
    	
    	Tranca tranca = getTrancaNova();
    	
    	HttpResponse<String> response = Unirest.get(baseUrl+"tranca/" + tranca.getId()).asString(); 
        assertEquals(200, response.getStatus());  
        assertEquals(JavalinJson.toJson(tranca), response.getBody());
    }
    
    @Test
    public void putTrancaTest() {
    	
    	Tranca tranca = getTrancaNova();
    	
    	HttpResponse<JsonNode> response = Unirest.put(baseUrl+"tranca/" + tranca.getId()).body(
    			"{\"numero\":\""+ 13 +"\", "
    			+ "\"localizacao\":\""+ tranca.getLocalizacao() +"\", "
    			+ "\"anoDeFabricacao\":\""+ tranca.getAnoDeFabricacao() +"\", "
    			+ "\"modelo\":\""+ tranca.getModelo() +"\", "
    			+ "\"status\":\""+ tranca.getStatus() +"\" "
    			+ "}").asJson();
    	
    	Tranca tranca2 = getTrancaNova();
    	tranca2.setId(tranca.getId());
    	tranca2.setNumero(13);
    	
        assertEquals(200, response.getStatus());  
        assertEquals(JavalinJson.toJson(tranca2), response.getBody().toString());
    } 
    
    @Test
    public void deleteTrancaTest() {
        
    	Tranca tranca = getTrancaNova();
    	
    	HttpResponse<String> response = Unirest.delete(baseUrl+"tranca/" + tranca.getId()).asString(); 
        assertEquals(200, response.getStatus());
    }
    
    @Test
    public void alterarStatusTrancaTest() {
    	
    	Tranca tranca = getTrancaNova();
    	
    	HttpResponse<String> response = Unirest.post(baseUrl+"tranca/{idTranca}/status/{acao}")
        .routeParam("idTranca", tranca.getId())
        .routeParam("acao", "OCUPADA")
        .asString();
        
    	Tranca tranca2 = getTrancaNova();
    	tranca2.setId(tranca.getId());
    	tranca2.setStatus(StatusT.OCUPADA);
    	
    	assertEquals(200, response.getStatus());  
        assertEquals(JavalinJson.toJson(tranca2), response.getBody());
    }
    
    @Test
    public void getBicicletaTrancaTest() {
    	
    	Tranca tranca = getTrancaNova();
    	Bicicleta bike = getBicicletaNova();
    	
    	tranca.setBicicleta(bike.getId());
    	
    	HttpResponse<String> response = Unirest.get(baseUrl+"tranca/" + tranca.getId() + "/bicicleta").asString(); 
    	
        assertEquals(200, response.getStatus());  
        assertEquals(JavalinJson.toJson(bike), response.getBody());
    }  
    
    @Test
    public void cadastraTrancaTest() {
    	
    	Tranca tra = getTrancaNova();
    	
    	HttpResponse<JsonNode> response = Unirest.post(baseUrl+"tranca").body(
    			"{\"numero\":\""+ 25 +"\", "
    			+ "\"localizacao\":\""+ tra.getLocalizacao() +"\", "
    			+ "\"anoDeFabricacao\":\""+ tra.getAnoDeFabricacao() +"\", "
    			+ "\"modelo\":\""+ tra.getModelo() +"\", "
    			+ "\"status\":\""+ tra.getStatus() +"\" "
    			+ "}").asJson();
    	
    	Tranca tra2 = getTrancaNova();
    	tra2.setStatus(StatusT.NOVA);
    	//Colocar o mesmo id do que foi criado no post
    	Tranca tranca = TrancaController.trancas.get(TrancaController.trancas.size()-2);	
    	tra2.setId(tranca.getId());
    	
    	assertEquals(200, response.getStatus());
        assertEquals(JavalinJson.toJson(tra2), response.getBody().toString());
    }
    
    @Test
    public void integraTrancaNaRedeTest() {
        
    	Tranca tra = getTrancaNova();
    	Totem tot = getTotemNovo();
    	
    	tra.setStatus(StatusT.NOVA);
    	
    	HttpResponse<JsonNode> response = Unirest.post(baseUrl+"tranca/integrarNaRede").body(
    			"{\"idTotem\":\""+ tot.getId() +"\", "
    			+ "\"idTranca\":\""+ tra.getId() +"\" "
    			+ "}").asJson();
    	
        assertEquals(200, response.getStatus());
    }
    
    @Test
    public void retiraTrancaDaRedeTest() {
    	
    	Tranca tra = getTrancaNova();
    	Totem tot = getTotemNovo();
    	
    	tra.setStatus(StatusT.NOVA);
    	
    	HttpResponse<JsonNode> response = Unirest.post(baseUrl+"tranca/retirarDaRede").body(
    			"{\"idTotem\":\""+ tot.getId() +"\", "
    			+ "\"idTranca\":\""+ tra.getId() +"\" "
    			+ "}").asJson();
    	
        assertEquals(200, response.getStatus());
    }
    
    @Test
    public void recuperaTotensTest() {
    	
    	getTotemNovo();
    	
    	HttpResponse<String> response = Unirest.get(baseUrl+"totem").asString();        
        assertEquals(200, response.getStatus());  
        assertEquals(JavalinJson.toJson(TotemController.totens), response.getBody());
    } 
    
    @Test
    public void putTotemTest() {

    	Totem totem = getTotemNovo();
    	
    	HttpResponse<JsonNode> response = Unirest.put(baseUrl+"totem/" + totem.getId()).body(
    			"{\"localizacao\":\""+ "2.22, 7.11" +"\" "
    			+ "}").asJson();
    	
    	Totem totem2 = getTotemNovo();
    	totem2.setId(totem.getId());
    	totem2.setLocalizacao("2.22, 7.11");
    	
        assertEquals(200, response.getStatus());  
        assertEquals(JavalinJson.toJson(totem2), response.getBody().toString());
    } 
    
    @Test
    public void deleteTotemTest() {
        
    	Totem totem = getTotemNovo();
    	
    	HttpResponse<String> response = Unirest.delete(baseUrl+"totem/" + totem.getId()).asString(); 
        assertEquals(200, response.getStatus());
    }
    
    @Test
    public void cadastraTotemTest() {
    	
    	Totem totem = getTotemNovo();
    	
    	HttpResponse<JsonNode> response = Unirest.post(baseUrl+"totem").body(
    			"{\"localizacao\":\""+ totem.getLocalizacao() +"\" "
    			+ "}").asJson();
    	
    	Totem totem2 = getTotemNovo();
    	//Colocar o mesmo id do que foi criado no post
    	Totem tot = TotemController.totens.get(TotemController.totens.size()-2);	
    	totem2.setId(tot.getId());
    	
    	assertEquals(200, response.getStatus());
        assertEquals(JavalinJson.toJson(totem2), response.getBody().toString());
    }
    
    @Test
    public void getTrancasTotemTest() {
    	
    	TrancaController.trancas.clear();
    	
    	Totem totem = getTotemNovo();
    	Tranca tranca = getTrancaNova();
    	
    	HttpResponse<String> response = Unirest.get(baseUrl+"totem/" + totem.getId() + "/trancas").asString(); 
    	
    	ArrayList<Tranca> trancasTotem = new ArrayList<Tranca>();
    	trancasTotem.add(tranca);
    	
        assertEquals(200, response.getStatus());  
        assertEquals(JavalinJson.toJson(trancasTotem), response.getBody());
    }  
    
    @Test
    public void getBicicletasTotemTest() {
    	    	    	
    	Totem totem = getTotemNovo();
    	Tranca tranca = getTrancaNova();
    	Bicicleta bicicleta = getBicicletaNova();
    	
    	tranca.setBicicleta(bicicleta.getId());
    	
    	HttpResponse<String> response = Unirest.get(baseUrl+"totem/" + totem.getId() + "/bicicletas").asString(); 
    	
    	ArrayList<Bicicleta> bicicletasTotem = new ArrayList<Bicicleta>();
    	bicicletasTotem.add(bicicleta);
    	
        assertEquals(200, response.getStatus());  
        assertEquals(JavalinJson.toJson(bicicletasTotem), response.getBody());
    }
    
    /*----- ERROS (aumentar cobertura Sonar) ---- */

    @Test
    public void retiraTrancaDaRedeErroTest() {
    	
    	Tranca tra = getTrancaNova();
    	Totem tot = getTotemNovo();
    	
    	tra.setStatus(StatusT.APOSENTADA);
    	
    	HttpResponse<JsonNode> response = Unirest.post(baseUrl+"tranca/retirarDaRede").body(
    			"{\"idTotem\":\""+ tot.getId() +"\", "
    			+ "\"idTranca\":\""+ tra.getId() +"\" "
    			+ "}").asJson();
    	
        assertEquals(422, response.getStatus());
    }
    
    @Test
    public void integraTrancaNaRedeErroTest() {
        
    	Tranca tra = getTrancaNova();
    	Totem tot = getTotemNovo();
    	
    	tra.setStatus(StatusT.APOSENTADA);
    	
    	HttpResponse<JsonNode> response = Unirest.post(baseUrl+"tranca/integrarNaRede").body(
    			"{\"idTotem\":\""+ tot.getId() +"\", "
    			+ "\"idTranca\":\""+ tra.getId() +"\" "
    			+ "}").asJson();
    	
        assertEquals(422, response.getStatus());
    }
    
    @Test
    public void integraBicicletaNaRedeErroTest() {
        
    	Bicicleta bike = getBicicletaNova();
    	Tranca tra = getTrancaNova();
    	
    	tra.setBicicleta(bike.getId());
    	tra.setStatus(StatusT.OCUPADA);
    	bike.setStatus(Status.NOVA);
    	
    	HttpResponse<JsonNode> response = Unirest.post(baseUrl+"bicicleta/integrarNaRede").body(
    			"{\"idBicicleta\":\""+ bike.getId() +"\", "
    			+ "\"idTranca\":\""+ tra.getId() +"\" "
    			+ "}").asJson();
    	
        assertEquals(422, response.getStatus());
    }
    
    @Test
    public void getBicicletaErroTest() {
    	
    	Bicicleta bike = getBicicletaNova();
    	
    	HttpResponse<String> response = Unirest.get(baseUrl+"bicicleta/" + "4").asString(); 
        assertEquals(404, response.getStatus());
    }
    
    /*---------- Objetos -----------*/
    
    public static Bicicleta getBicicletaNova() {
		Year year = Year.of(2017); 
		Bicicleta bike = new Bicicleta("caloi", "vulcan", year, 324, Status.DISPONIVEL);
		BicicletaController.bicicletas.add(bike);
		return bike;
    }
    
    public static Tranca getTrancaNova() {
		Tranca tranca = new Tranca(25, "2.00, 7.00", "2012", "Basico", StatusT.NOVA);
		TrancaController.trancas.add(tranca);
		return tranca;
    }
    
    public static Totem getTotemNovo () {
		Totem totem = new Totem("2.00, 7.00");
		TotemController.totens.add(totem);
		return totem;
    }
    
    
}
